/*
 * Knopf.h
 *
 *  Created on: 06.08.2018
 *      Author: Joel
 */

#ifndef SRC_KNOPF_H_
#define SRC_KNOPF_H_
#include "Knopf.cpp"

class Knopf {
public:
	void Anzeigen()
	{
		sf::Texture Textur;
		Textur.loadFromFile("src\Schalter.png");

		sf::Sprite sprite(Textur);
	}
};

#endif /* SRC_KNOPF_H_ */
