/*
 * Start.cpp
 *
 *  Created on: 05.08.2018
 *      Author: Joel
 */

#include "Start.h"
#include <iostream>
#include <SFML/Graphics.hpp>
#include <Knopf.h>

using namespace std;

void Start()
{
	sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
    sf::RenderWindow window(sf::VideoMode(desktop.width, desktop.height), "XXL M�BA SPIELEPARADIES", sf::Style::Fullscreen);
    bool isFullscreen = true;
	Knopf Start;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
	    {
			switch (event.type)
			{
				case sf::Event::KeyReleased:
					switch (event.key.code)
					{
					case sf::Keyboard::Return:
						if (true == isFullscreen)
						{
							window.create(sf::VideoMode(desktop.width, desktop.height), "XXL M�BA SPIELEPARADIES", sf::Style::Default);
							isFullscreen = false;
						}
						else
						{
							window.create(sf::VideoMode(desktop.width, desktop.height), "XXL M�BA SPIELEPARADIES", sf::Style::Fullscreen);
							isFullscreen = true;
						}
					break;
					case sf::Keyboard::Escape:
					window.close();
					}
				break;
			}
	        if (event.type == sf::Event::Closed)
	        window.close();
	    }

		Start.Anzeigen();
	}

}

